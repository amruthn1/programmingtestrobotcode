package frc.robot.subsystems;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.motorcontrol.*;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class DriveSubsystem extends SubsystemBase{

    private DifferentialDrive m_robot;
    private Joystick m_stickLeft;
    private Joystick m_stickRight;

    private MotorController m_frontLeftMotorController;
    private MotorController m_frontRightMotorController;
    private MotorController m_rearLeftMotorController;
    private MotorController m_rearRighMotorController;

    private MotorControllerGroup m_right;
    private MotorControllerGroup m_left;

    public DriveSubsystem() {
        m_frontLeftMotorController = new PWMSparkMax(0);
        m_frontRightMotorController = new PWMSparkMax(1);
        m_rearLeftMotorController = new PWMSparkMax(2);
        m_rearRighMotorController = new PWMSparkMax(3);

        m_right = new MotorControllerGroup(m_frontRightMotorController, m_rearRighMotorController);
        m_left = new MotorControllerGroup(m_frontLeftMotorController, m_rearLeftMotorController);
        m_right.setInverted(true);
        
        m_robot = new DifferentialDrive(m_left, m_right);

        m_stickLeft = new Joystick(0);
        m_stickRight = new Joystick(1);
    }

    public void drive() {
        m_robot.tankDrive(m_stickLeft.getY(), m_stickRight.getY());
    }

    //spins robot clockwise
    public void spin() {
        m_robot.tankDrive(1.0, -1.0);
    }

    public void stop() {
        m_robot.stopMotor();
    }

}
