package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;

public class RobotSpin extends CommandBase{
    
    private DriveSubsystem driveSys;

    
    public RobotSpin(DriveSubsystem drive) {
      
        driveSys = drive;
        withTimeout(15); // stops spinning after 5 seconds
    }
    
    @Override
    public void initialize() {
        
        driveSys.spin();
    }

    @Override
    public void end(boolean interrupted) {
        // timer interrupts when it's over, stops spinning
        driveSys.stop();
    }
}
